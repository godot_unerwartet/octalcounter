LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ARCHITECTURE arch_top_level OF top_level IS

  COMPONENT cntr
    PORT (
    reset_n		: STD_LOGIC;
    clk_50		: STD_LOGIC;
    pb_in		: STD_LOGIC_VECTOR(1 DOWNTO 0);
    sw_in		: STD_LOGIC_VECTOR(9 DOWNTO 0);
    cntr0_out	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr1_out	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr2_out	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr3_out	: STD_LOGIC_VECTOR(3 DOWNTO 0)
    );
  END COMPONENT;
  
  COMPONENT ioctrl
  PORT (
    reset_n		: STD_LOGIC;
    clk_50		: STD_LOGIC;
    pb_in		: STD_LOGIC_VECTOR(1 DOWNTO 0);
    sw_in		: STD_LOGIC_VECTOR(9 DOWNTO 0);	
    pb_clean_out: STD_LOGIC_VECTOR(1 DOWNTO 0);
    sw_clean_out: STD_LOGIC_VECTOR(9 DOWNTO 0);
    cntr0_in	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr1_in	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr2_in	: STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr3_in	: STD_LOGIC_VECTOR(3 DOWNTO 0);	
    ss0_out		: STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss1_out		: STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss2_out		: STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss3_out		: STD_LOGIC_VECTOR(7 DOWNTO 0)  
    );
  END COMPONENT;

  SIGNAL s_connect_pb		: STD_LOGIC_VECTOR(1 DOWNTO 0);  -- to connect the push buttons of ioctrl and cntr unit
  SIGNAL s_connect_sw		: STD_LOGIC_VECTOR(9 DOWNTO 0);  -- to connect the switches of ioctrl and cntr unit
  SIGNAL s_connect_cntr0	: STD_LOGIC_VECTOR(3 DOWNTO 0);  -- to connect the counter digits to the ioctrl
  SIGNAL s_connect_cntr1	: STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL s_connect_cntr2	: STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL s_connect_cntr3	: STD_LOGIC_VECTOR(3 DOWNTO 0); 	
  SIGNAL s_connect_ss0	: STD_LOGIC_VECTOR(7 DOWNTO 0);  -- to connect the counter digits to the ioctrl
  SIGNAL s_connect_ss1	: STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL s_connect_ss2	: STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL s_connect_ss3	: STD_LOGIC_VECTOR(7 DOWNTO 0);
	
  BEGIN	
    i_cntr : cntr
    PORT MAP (
      reset_n	=> reset_n,
      clk_50	=> clk_50,
      pb_in		=> s_connect_pb,
      sw_in		=> s_connect_sw,
      cntr0_out	=> s_connect_cntr0,
      cntr1_out	=> s_connect_cntr1,
      cntr2_out	=> s_connect_cntr2,
      cntr3_out	=> s_connect_cntr3
      );
	
    i_ioctrl : ioctrl
    PORT MAP (
      reset_n		=> reset_n,
      clk_50		=> clk_50,
      pb_in			=> pb_in,
      sw_in			=> sw_in,
      pb_clean_out	=> s_connect_pb,
      sw_clean_out	=> s_connect_sw,
      cntr0_in		=> s_connect_cntr0,
      cntr1_in		=> s_connect_cntr1,
      cntr2_in		=> s_connect_cntr2,
      cntr3_in		=> s_connect_cntr3,	  
      ss0_out		=> s_connect_ss0,
      ss1_out		=> s_connect_ss1,
      ss2_out		=> s_connect_ss2,
      ss3_out		=> s_connect_ss3
	  );
	
END arch_top_level;

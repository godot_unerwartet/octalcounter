LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ARCHITECTURE arch_ioctrl OF ioctrl IS
BEGIN
  -- process decoding binary coded octal numbers in 8 segment output (negative active)
  p_decode_segments : PROCESS(cntr0_in, cntr1_in, cntr2_in, cntr3_in)
  BEGIN 
    CASE cntr0_in IS
    WHEN "0000" =>
      ss0_out <= "00000011";
    WHEN "0001" =>
      ss0_out <= "10011111";
    WHEN "0010" =>
      ss0_out <= "00100101";
    WHEN "0011" =>
      ss0_out <= "00001101";
    WHEN "0100" =>
      ss0_out <= "10011001";
    WHEN "0101" =>
      ss0_out <= "01001001";
    WHEN "0110" =>
      ss0_out <= "01000000";
    WHEN "0111" =>
      ss0_out <= "00011111";
    WHEN OTHERS =>     -- this case should never happen
	  ss0_out <= "00000000";
	END CASE;
  END PROCESS p_decode_segments;
  -- process cleaning the board input from switches and push buttons
  p_clean_input : PROCESS(sw_in, pb_in)
  BEGIN
    -- TODO add 2 DFF
	-- TODO debouncing
  END PROCESS p_clean_input;
END arch_ioctrl;

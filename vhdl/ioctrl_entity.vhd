LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ioctrl IS
  PORT(
    reset_n		:  IN	STD_LOGIC;
    clk_50		:  IN	STD_LOGIC;
    pb_in		:  IN	STD_LOGIC_VECTOR(1 DOWNTO 0);
    sw_in		:  IN	STD_LOGIC_VECTOR(9 DOWNTO 0);	
    cntr0_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr1_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr2_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
    cntr3_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);	
    ss0_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss1_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss2_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
    ss3_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0); 
    pb_clean_out:  OUT	STD_LOGIC_VECTOR(1 DOWNTO 0);
    sw_clean_out:  OUT	STD_LOGIC_VECTOR(9 DOWNTO 0)
    );
END ioctrl;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ARCHITECTURE arch_cntr OF cntr IS
  SIGNAL s_enable_10 : std_logic := '0';
BEGIN
  -- process to generate enable signal - counting rate 10 Hz
  p_prescaler : PROCESS (clk_50)
  VARIABLE v_count_to_5000000 : INTEGER := 0;   -- ratio of clock and enable 
  BEGIN
    IF clk_50'event AND clk_50 = '1' THEN	-- synchronizing the process to the clock
      IF v_count_to_5000000 = 5000000 THEN   -- set enable and reset counting variable
		s_enable_10 <= '1';
        v_count_to_5000000 := 0;   
	  ELSE           -- counting up
	    v_count_to_5000000 := v_count_to_5000000 + 1;
      END IF;  
    END IF;
    
  END PROCESS p_prescaler;
  -- counting process - the functional heart of the project
  p_decode_switch : PROCESS (sw_in)	
  BEGIN
    -- TODO switch-input auf die noch zu definierenden signale fuer up und down und hold abbilden
  END PROCESS;  
  -- counting process - the functional heart of the project
  p_counter : PROCESS (clk_50, s_enable_10, reset_n)	
  BEGIN
  
  END PROCESS;
END arch_cntr;

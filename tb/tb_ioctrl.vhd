-------------------------------------------------------------------------------
--                                                                      
--                        Seven Segment Decoder Class - Testbench
--
--
-------------------------------------------------------------------------------
--                                                                      
-- ENTITY:         tb_seven_segment_decoder
--
-- FILENAME:       tb_seven_segment_decoder_.vhd
-- 
-- ARCHITECTURE:   sim
-- 
-- ENGINEER:       Gudrun Schuchmann, Raphael Siebenhofer
--
-- DATE:           28. October
--
-- VERSION:        1.0
--
-------------------------------------------------------------------------------
--                                                                      
-- DESCRIPTION:    This is the entity and architecture declaration of the ioctrl testbench
--
--
-------------------------------------------------------------------------------
--
-- REFERENCES:     (none)
--
-------------------------------------------------------------------------------
--                                                                      
-- PACKAGES:       std_logic_1164 (IEEE library)
--
-------------------------------------------------------------------------------
--                                                                      
-- CHANGES:        (none)
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

-- entity
ENTITY tb_ioctrl IS 
END tb_ioctrl;




-- architecture
ARCHITECTURE sim_ioctrl OF tb_ioctrl IS

	COMPONENT ioctrl IS
		PORT(
			reset_n		:  IN	STD_LOGIC;
			clk_50		:  IN	STD_LOGIC;
			pb_in		:  IN	STD_LOGIC_VECTOR(1 DOWNTO 0);
			sw_in		:  IN	STD_LOGIC_VECTOR(9 DOWNTO 0);	
			cntr0_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
			cntr1_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
			cntr2_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
			cntr3_in	:  IN	STD_LOGIC_VECTOR(3 DOWNTO 0);	
			ss0_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
			ss1_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
			ss2_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
			ss3_out		:  OUT	STD_LOGIC_VECTOR(7 DOWNTO 0); 
			pb_clean_out:  OUT	STD_LOGIC_VECTOR(1 DOWNTO 0);
			sw_clean_out:  OUT	STD_LOGIC_VECTOR(9 DOWNTO 0)			
		);
	END COMPONENT;
	
	-- Declare the signals used stimulating the design's inputs.
	SIGNAL s_reset_n		:	STD_LOGIC;
	SIGNAL s_clk_50			:	STD_LOGIC;	
	SIGNAL s_pb_in			:  	STD_LOGIC_VECTOR(1 DOWNTO 0) := "UU";
	SIGNAL s_sw_in			:  	STD_LOGIC_VECTOR(9 DOWNTO 0) := "UUUUUUUUUU";
	SIGNAL s_cntr0_in		:	STD_LOGIC_VECTOR(3 DOWNTO 0) := "UUUU";
	SIGNAL s_cntr1_in		:	STD_LOGIC_VECTOR(3 DOWNTO 0) := "UUUU";
	SIGNAL s_cntr2_in		:	STD_LOGIC_VECTOR(3 DOWNTO 0) := "UUUU";
	SIGNAL s_cntr3_in		:	STD_LOGIC_VECTOR(3 DOWNTO 0) := "UUUU";	
	SIGNAL s_ss0_out		:	STD_LOGIC_VECTOR(7 DOWNTO 0) := "UUUUUUUU";
	SIGNAL s_ss1_out		:	STD_LOGIC_VECTOR(7 DOWNTO 0) := "UUUUUUUU";
	SIGNAL s_ss2_out		:	STD_LOGIC_VECTOR(7 DOWNTO 0) := "UUUUUUUU";
	SIGNAL s_ss3_out		:	STD_LOGIC_VECTOR(7 DOWNTO 0) := "UUUUUUUU";	
	SIGNAL s_pb_clean_out	:	STD_LOGIC_VECTOR(1 DOWNTO 0) := "UU";
	SIGNAL s_sw_clean_out	:	STD_LOGIC_VECTOR(9 DOWNTO 0) := "UUUUUUUUUU";
	
BEGIN


	-- Instantiate the rtl
	i_ioctrl : ioctrl
	PORT MAP              
	(
		reset_n	 => s_reset_n,
		clk_50 => s_clk_50,
		pb_in => s_pb_in,		-- left: port - right: signal
		sw_in => s_sw_in,
		cntr0_in => s_cntr0_in,
		cntr1_in => s_cntr1_in,
		cntr2_in => s_cntr2_in,
		cntr3_in => s_cntr3_in,
		ss0_out => s_ss0_out,
		ss1_out => s_ss1_out,
		ss2_out => s_ss2_out,
		ss3_out => s_ss3_out,
		pb_clean_out => s_pb_clean_out,
		sw_clean_out => s_sw_clean_out
	);

	-- Beginning of test process
	p_test_input_clean : PROCESS  
	
	
    BEGIN	
	
	s_pb_in <= "11";		-- expected: pb_clean_out: "11"
	WAIT FOR 50 ps;
	
	s_pb_in <= "00";		-- expected: pb_clean_out: "11"
	WAIT FOR 50 ps;

	s_sw_in <= "1111111111";		-- expected: pb_clean_out: "1111111111"
	WAIT FOR 50 ps;
	
	s_sw_in <= "0000000000";		-- expected: pb_clean_out: "0000000000"
	WAIT FOR 50 ps;

	END PROCESS;
	
	p_test_decoder : PROCESS  
	
    BEGIN
		s_cntr0_in <= "0000";		-- expected: s_ss0_out: "00000011"
		WAIT FOR 50 ps;
		s_cntr0_in <= "0110";		-- expected: s_ss0_out: "01000000"
		WAIT FOR 50 ps;

	END PROCESS;
	
END sim_ioctrl;